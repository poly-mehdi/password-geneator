/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    colors: {
      'dark-green': '#A4FFAF',
      'dark-red': '#F64A4A',
      'dark-orange': '#FB7C58',
      'dark-yellow': '#F8CD65',
      'light-dark': '#24232C',
      'dark-white': '#E6E5EA',
      'dark-grey': '#817D92',
      dark: '#18171F',
    },
    fontFamily: {
      jetbrains: ['JetBrains Mono', 'monospace'],
    },
  },
  plugins: [],
}
