export const generateRandomPassword = (
  length,
  includeUppercase,
  includeLowercase,
  includeNumbers,
  includeSymbols
) => {
  // Define character sets to use in password
  const uppercaseChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  const lowercaseChars = 'abcdefghijklmnopqrstuvwxyz'
  const numberChars = '0123456789'
  const symbolChars = '!@#$%^&*()_+[]{}|;:,.<>?'

  // Define specific sets to use based on user input
  let set = ''

  if (includeUppercase) {
    set += uppercaseChars
  }

  if (includeLowercase) {
    set += lowercaseChars
  }

  if (includeNumbers) {
    set += numberChars
  }

  if (includeSymbols) {
    set += symbolChars
  }

  // Generate password
  let password = ''

  if (
    !includeUppercase &&
    !includeLowercase &&
    !includeNumbers &&
    !includeSymbols
  ) {
    return ''
  }
  for (let i = 0; i < length; i++) {
    password += set.charAt(Math.floor(Math.random() * set.length))
  }

  return password
}

export const setStrengthPassword = (
  includeUppercase,
  includeLowercase,
  includeNumbers,
  includeSymbols
) => {
  let strength = 0

  if (includeUppercase) {
    strength += 1
  }

  if (includeLowercase) {
    strength += 1
  }

  if (includeNumbers) {
    strength += 1
  }

  if (includeSymbols) {
    strength += 1
  }

  let message = ''

  strength == 0
    ? (message = 'TOO WEAK!')
    : strength == 1
    ? (message = 'TOO WEAK!')
    : strength == 2
    ? (message = 'WEAK')
    : strength == 3
    ? (message = 'MEDIUM')
    : (message = 'STRONG')

  return message
}
