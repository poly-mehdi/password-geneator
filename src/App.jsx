import { useState } from 'react'
import copyIcon from './assets/images/icon-copy.svg'
import arrowIcon from './assets/images/icon-arrow-right.svg'

import { generateRandomPassword, setStrengthPassword } from './utils'
import CopyToClipboard from 'react-copy-to-clipboard'

const App = () => {
  const [password, setPassword] = useState('')
  const [passwordLength, setPasswordLength] = useState(10)
  const [strength, setStrength] = useState('TOO WEAK!')

  const [includeUppercase, setIncludeUppercase] = useState(false)
  const [includeLowercase, setIncludeLowercase] = useState(false)
  const [includeNumbers, setIncludeNumbers] = useState(false)
  const [includeSymbols, setIncludeSymbols] = useState(false)

  const handleGeneratePassword = () => {
    setPassword(
      generateRandomPassword(
        passwordLength,
        includeUppercase,
        includeLowercase,
        includeNumbers,
        includeSymbols
      )
    )
    setStrength(
      setStrengthPassword(
        includeUppercase,
        includeLowercase,
        includeNumbers,
        includeSymbols
      )
    )
  }

  return (
    <main className="px-5 py-10 bg-dark h-screen font-jetbrains flex flex-col gap-y-3 lg:gap-y-5">
      <h2 className="text-dark-grey text-center">Password Generator</h2>
      <section className="py-6 px-6 w-full bg-light-dark flex relative lg:max-w-lg lg:mx-auto">
        {password !== '' ? (
          <p className="text-dark-white text-2xl font-bold tracking-wide h-6">
            {password}
          </p>
        ) : (
          <p className="text-dark-grey text-2xl font-bold tracking-wide h-6">
            P4$WOrD!
          </p>
        )}

        <CopyToClipboard text={password}>
          <img
            src={copyIcon}
            alt="copy-icon"
            className="absolute right-6 cursor-pointer text-dark-re hover:scale-105 ease-linear"
          />
        </CopyToClipboard>
      </section>
      <section className="py-6 px-6 w-full bg-light-dark flex flex-col gap-y-5 lg:max-w-lg lg:mx-auto">
        <div className="flex justify-between items-center">
          <p className="text-dark-white text-lg">Character Length</p>
          <span className="text-dark-green text-3xl">{passwordLength}</span>
        </div>
        <input
          type="range"
          min={1}
          max={20}
          value={passwordLength}
          onChange={(e) => setPasswordLength(e.target.value)}
          className="appearance-none h-3 bg-dark cursor-pointer range-thumb"
        />
        <div className="flex flex-col gap-y-3">
          <div className="flex gap-x-5 items-center">
            <input
              type="checkbox"
              checked={includeUppercase}
              onChange={() => setIncludeUppercase(!includeUppercase)}
            />
            <p className="text-dark-white text-lg">Include Uppercase Letters</p>
          </div>
          <div className="flex gap-x-5 items-center">
            <input
              type="checkbox"
              checked={includeLowercase}
              onChange={() => setIncludeLowercase(!includeLowercase)}
            />
            <p className="text-dark-white text-lg">Include Lowercase Letters</p>
          </div>
          <div className="flex gap-x-5 items-center">
            <input
              type="checkbox"
              className="checkbox"
              checked={includeNumbers}
              onChange={() => setIncludeNumbers(!includeNumbers)}
            />
            <p className="text-dark-white text-lg">Include Numbers</p>
          </div>
          <div className="flex gap-x-5 items-center">
            <input
              type="checkbox"
              className="checkbox"
              checked={includeSymbols}
              onChange={() => setIncludeSymbols(!includeSymbols)}
            />
            <p className="text-dark-white text-lg">Include Symbols</p>
          </div>
        </div>
        <div className="flex justify-between bg-dark px-3 py-4">
          <p className="text-dark-grey">STRENGTH</p>
          <div className="flex gap-3">
            <p className="capitalize text-dark-white">{strength}</p>
            {strength === 'TOO WEAK!' ? (
              <div className="flex gap-1">
                <div className=" bg-dark-red h-6 w-2"></div>
                <div className="border-2 border-dark-white h-6 w-2"></div>
                <div className="border-2 border-dark-white h-6 w-2"></div>
                <div className="border-2 border-dark-white h-6 w-2"></div>
              </div>
            ) : strength == 'WEAK' ? (
              <div className="flex gap-1">
                <div className=" bg-dark-orange h-6 w-2"></div>
                <div className="bg-dark-orange border-dark-white h-6 w-2"></div>
                <div className="border-2 border-dark-white h-6 w-2"></div>
                <div className="border-2 border-dark-white h-6 w-2"></div>
              </div>
            ) : strength == 'MEDIUM' ? (
              <div className="flex gap-1">
                <div className=" bg-dark-yellow h-6 w-2"></div>
                <div className="bg-dark-yellow border-dark-white h-6 w-2"></div>
                <div className="bg-dark-yellow border-dark-white h-6 w-2"></div>
                <div className="border-2 border-dark-white h-6 w-2"></div>
              </div>
            ) : (
              <div className="flex gap-1">
                <div className=" bg-dark-green h-6 w-2"></div>
                <div className=" bg-dark-green h-6 w-2"></div>
                <div className=" bg-dark-green h-6 w-2"></div>
                <div className=" bg-dark-green h-6 w-2"></div>
              </div>
            )}
          </div>
        </div>
        <button
          onClick={handleGeneratePassword}
          className="bg-dark-green px-3 py-4 flex gap-x-4 justify-center items-center cursor-pointer hover:bg-light-dark hover:text-dark-green hover:border-2"
        >
          <p>GENERATE</p>
          <img src={arrowIcon} alt="arrow" />
        </button>
      </section>
    </main>
  )
}
export default App
